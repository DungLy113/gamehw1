﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour {

	public int Width;
	public int Height;
	private int origin_x;
	private int origin_y;

	// Use this for initialization
	void Start () {
		Width = 200;
		Height = 50;
		origin_x = Screen.width / 2 - Width / 2;
		origin_y = Screen.height / 2 - Height * 2;
	}

	void OnGUI() {
		if(GUI.Button(new Rect(origin_x, origin_y, Width,Height), "Space Shooter")) {
			Application.LoadLevel(1);
		}
		if(GUI.Button(new Rect(origin_x, origin_y + Height + 10, Width, Height), "Rolling Ball")) {
			Application.LoadLevel(2);
		}
		if(GUI.Button(new Rect(origin_x, origin_y +Height * 2 + 20, Width, Height), "Exit")) {
			#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
			#else
			Application.Quit();
			#endif
		}
	}
}
